## 0.0.1

* FlytSwitch provides users with an easy way to create dynamic Widgets with dynamic content that can be modified and changed on the fly with no need to rebuild or republish the app.
