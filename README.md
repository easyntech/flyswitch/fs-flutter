Flyswitch Flutter Package
=========================

Flyswitch is a marketing tool that generates native stateful/less UI components that can be modified and published on the fly without requiring the app to be rebuilt.
The Flyswitch Flutter Package makes it simple for developers to connect to and use the Flyswitch service in their Flutter applications.

> **Warning**
>
> This package is still in development and is not yet ready for production use.
>
> Although we have tested it thoroughly, there might be some bugs that we haven't discovered yet.
>
> Please use it at your own risk.
>
> We will update this README file once the package is ready for production use.

Getting Started
---------------

To use Flyswitch in your Flutter application, take the following steps:

### At FlySwitch Platform

1. Create an account at [https://flyswitch.io](https://flyswitch.io/)
2. Create a new project if not already done, or use an existing one
3. Create the widget you wish to use in your app, or use an existing one
4. Design your widget using the Flyswitch UI Builder
5. Press deploy to publish your widget
6. Click the connect button to get your API key and widget ID

### Flutter Application

1. Import the Flyswitch package in your `main.dart` file:

```dart
import 'package:flyswitch/flyswitch.dart'
```

2. Initialize Flyswitch by adding the following code in the `main` function:

```dart
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // Initialize Flyswitch with your API key (obtained from Flyswitch Platform)
  FlySwitch.init("API_KEY");
  runApp(MyApp());
}
```

#### To show the widget, you can do either of the following:

* Load Then Show

```dart
  // Load the widget
  // This is optional, if you don't load the widget, it will be loaded automatically when you call the show method
  // But if you want to load the widget before showing it, you can use this method
  // This is useful if you want to show the widget after a specific event (e.g. user clicks a button)
  await FlySwitch.i.load("WIDGET_ID")

  // Show the widget
  // This will show the widget if it's already loaded, otherwise it will load the widget first then show it
  FlySwitch.i.show("WIDGET_ID")
```

* Directly use the show method to load and show the widget

```dart
  // Show the widget
  // This will load the widget first then show it if it's not loaded yet or show it directly if it's already loaded
  FlySwitch.i.show("WIDGET_ID")
```

Documentation
-------------
<!-- You can find the full documentation at [https://docs.flyswitch.io](https://docs.flyswitch.io). -->
Coming Soon

Support
-------

If you have any questions or issues with Flyswitch, please don't hesitate to contact us at <support@flyswitch.io>.
or, push an issue on the [GitHub repository](https://gitlab.com/easyntech/flyswitch/fs-flutter).
