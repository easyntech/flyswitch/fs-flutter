library flyswitch;

import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/models/initialization.model.dart';
import 'package:flyswitch/src/service/flyswitch.service.dart';

class FlySwitch {
  static init(String apiKey) {
    setupLocator();
    locator<FlySwitchService>().init(apiKey);
  }

  static FlySwitchService get i {
    return locator<FlySwitchService>();
  }
}
