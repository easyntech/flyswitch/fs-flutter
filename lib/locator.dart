import 'package:flyswitch/src/loaders/widget/widget_loader.controller.dart';
import 'package:flyswitch/src/service/api.service.dart';
import 'package:flyswitch/src/service/flyswitch.service.dart';
import 'package:flyswitch/src/service/json_registery.service.dart';
import 'package:flyswitch/src/service/navigation.service.dart';
import 'package:flyswitch/src/service/notifier.service.dart';
import 'package:flyswitch/src/service/widgets.service.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

setupLocator() {
  locator.registerLazySingleton(() => NotifierService());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => API());
  locator.registerLazySingleton(() => WidgetsService());
  locator.registerLazySingleton(() => FlySwitchService());
  locator.registerLazySingleton(() => JsonRegistryService());

  locator.registerFactory(() => WidgetLoaderController());
}
