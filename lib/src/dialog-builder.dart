import 'package:child_builder/src/child_widget_builder.dart';
import 'package:flutter/material.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';

class DialogBuilder extends JsonWidgetBuilder {
  final double elevation;
  final String backgroundColor;
  final double insetPadding;

  DialogBuilder({
    this.elevation = 0,
    this.backgroundColor = "transparent",
    this.insetPadding = 0,
  }) : super(numSupportedChildren: 1);

  static const type = 'dialog';

  static DialogBuilder fromDynamic(
    dynamic map, {
    JsonWidgetRegistry? registry,
  }) {
    DialogBuilder result;

    result = DialogBuilder(
      elevation: map['elevation'] ?? 0.0,
      backgroundColor: map['backgroundColor'] ?? "transparent",
      insetPadding: map['insetPadding'] ?? 0.0,
    );
    return result;
  }

  @override
  Widget buildCustom({
    ChildWidgetBuilder? childBuilder,
    required BuildContext context,
    required JsonWidgetData data,
    Key? key,
  }) {
    // assert(
    //   data.children?.isNotEmpty != true,
    //   '[SvgBuilder] does not support children.',
    // );
    return Dialog(
      elevation: elevation,
      backgroundColor: backgroundColor == "transparent"
          ? Colors.transparent
          : Color(int.parse("0xFF${backgroundColor.replaceAll("#", '')}")),
      insetPadding: EdgeInsets.all(insetPadding),
      child: data.children?.isNotEmpty == true
          ? data.children![0].build(
              childBuilder: childBuilder,
              context: context,
            )
          : Container(),
    );
  }
}
