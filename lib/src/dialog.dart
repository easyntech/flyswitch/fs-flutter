import 'dart:ui';

import 'package:flutter/material.dart';

class LaunchScreenDialog extends StatefulWidget {
  final Function()? handleButtonClick;
  const LaunchScreenDialog({
    super.key,
    this.handleButtonClick,
  });
  @override
  State<LaunchScreenDialog> createState() => _LaunchScreenDialogState();
}

class _LaunchScreenDialogState extends State<LaunchScreenDialog> {
  late final SingletonFlutterWindow window;
  double dpr = 1;
  Size size = Size.zero;

  double screenWidth = 0;
  double screenHeight = 0;

  double playgroundWidth = 375;
  double playgroundHeight = 665;

  int gridColumns = 100;
  int gridRows = 100;

  double cellWidth = 0;
  double cellHeight = 0;

  @override
  void initState() {
    super.initState();
    window = WidgetsBinding.instance.window;
    dpr = window.devicePixelRatio;
    size = window.physicalSize / window.devicePixelRatio;
    screenWidth = size.width;
    screenHeight = size.height;

    double swToPw = (screenWidth) / playgroundWidth;
    double shToPh = (screenHeight) / playgroundHeight;

    if (shToPh < 1) {
      gridRows = (gridRows * shToPh).toInt();
    }

    if (swToPw < 1) {
      gridColumns = (gridColumns * swToPw).toInt();
    }

    cellHeight = getVerticalSize(playgroundHeight / gridRows * shToPh);
    cellWidth = getHorizontalSize(playgroundWidth / gridColumns * swToPw);
  }

  double getHorizontalSize(double px) => px * (size.width / screenWidth);

  double getVerticalSize(double px) {
    num statusBar = MediaQueryData.fromWindow(window).viewPadding.top;
    num sh = size.height - statusBar;
    return px * (sh / screenHeight);
  }

  double getSize(double px) {
    final height = getVerticalSize(px);
    final width = getHorizontalSize(px);

    if (height < width) {
      return height.toInt().toDouble();
    } else {
      return width.toInt().toDouble();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.zero,
      child: Stack(
        children: [
          Positioned(
            top: 19 * cellHeight,
            left: 4 * cellWidth,
            child: Container(
              width: 92 * cellWidth,
              height: 64 * cellHeight,
              padding: const EdgeInsets.only(
                top: 10,
                bottom: 10,
                left: 10,
                right: 10,
              ),
              decoration: const BoxDecoration(
                image: null,
                color: Color(0xFFf4f4f4),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
                border: null,
              ),
              child: null,
            ),
          ),
          Positioned(
            top: 22 * cellHeight,
            left: 26 * cellWidth,
            child: Container(
              width: 48 * cellWidth,
              height: 23 * cellHeight,
              padding: const EdgeInsets.only(
                top: 5,
                bottom: 5,
                left: 5,
                right: 5,
              ),
              decoration: const BoxDecoration(
                image: null,
                color: Color(0xFF8000ff),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(100),
                  topRight: Radius.circular(100),
                  bottomLeft: Radius.circular(100),
                  bottomRight: Radius.circular(100),
                ),
                border: const Border(
                  top: BorderSide(color: Color(0xFF000000), width: 1),
                  bottom: BorderSide(color: Color(0xFF000000), width: 1),
                  left: BorderSide(color: Color(0xFF000000), width: 1),
                  right: BorderSide(color: Color(0xFF000000), width: 1),
                ),
              ),
              child: null,
            ),
          ),
          Positioned(
            top: (48) * cellHeight,
            left: (29) * cellWidth,
            child: Container(
              width: (41) * cellWidth,
              height: (3) * cellHeight,
              padding: const EdgeInsets.only(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
              ),
              decoration: const BoxDecoration(
                image: null,
                color: Colors.transparent,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                border: null,
              ),
              child: Text('SPECIAL OFFER',
                  style: TextStyle(
                    color: Color(0xFF000000),
                    fontSize: getSize(14),
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    decoration: null,
                    letterSpacing: 0,
                  ),
                  textAlign: TextAlign.center),
            ),
          ),
          Positioned(
            top: 52 * cellHeight,
            left: 16 * cellWidth,
            child: Container(
              width: 67 * cellWidth,
              height: 11 * cellHeight,
              padding: const EdgeInsets.only(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
              ),
              decoration: const BoxDecoration(
                image: null,
                color: Colors.transparent,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                border: null,
              ),
              child: Text('50% OFF',
                  style: TextStyle(
                    color: Color(0xFFff7b00),
                    fontSize: getSize(45),
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                    decoration: null,
                    letterSpacing: 0,
                  ),
                  textAlign: TextAlign.center),
            ),
          ),
          Positioned(
            top: 63 * cellHeight,
            left: 8 * cellWidth,
            child: Container(
              width: 83 * cellWidth,
              height: 8 * cellHeight,
              padding: const EdgeInsets.only(
                top: 5,
                bottom: 5,
                left: 5,
                right: 5,
              ),
              decoration: const BoxDecoration(
                image: null,
                color: Colors.transparent,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                border: null,
              ),
              child:
                  Text('Get a discount on all our products in your next order',
                      style: TextStyle(
                        color: Color(0xFF000000),
                        fontSize: getSize(12),
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        decoration: null,
                        letterSpacing: 0,
                      ),
                      textAlign: TextAlign.center),
            ),
          ),
          Positioned(
            top: 73 * cellHeight,
            left: 36 * cellWidth,
            child: ElevatedButton(
              onPressed: widget.handleButtonClick,
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.only(
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                ),
                minimumSize: Size(26 * cellWidth, 4 * cellHeight),
                maximumSize: Size(26 * cellWidth, 4 * cellHeight),
                backgroundColor: Color(0xFF00e0f0),
                foregroundColor: Color(0xFF000000),
                shape: const RoundedRectangleBorder(
                  side: BorderSide.none,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
              ),
              child: Text('GET IT',
                  style: TextStyle(
                    color: Color(0xFF000000),
                    fontSize: getSize(14),
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    decoration: null,
                    letterSpacing: 0,
                  ),
                  textAlign: TextAlign.center),
            ),
          ),
          Positioned(
            top: 19 * cellHeight,
            left: 54 * cellWidth,
            child: Container(
              width: 31 * cellWidth,
              height: 15 * cellHeight,
              padding: const EdgeInsets.only(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
              ),
              decoration: const BoxDecoration(
                image: const DecorationImage(
                  image: NetworkImage(
                      'http://localhost:4000/static/upload/637969248afcffafb987d951//637969248afcffafb987d951-pJ-aWJ-party.png'),
                  fit: BoxFit.cover,
                ),
                color: null,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                border: null,
              ),
              child: null,
            ),
          ),
          Positioned(
            top: 24 * cellHeight,
            left: 29 * cellWidth,
            child: Container(
              width: 40 * cellWidth,
              height: 18 * cellHeight,
              padding: const EdgeInsets.only(
                top: 5,
                bottom: 5,
                left: 5,
                right: 5,
              ),
              decoration: const BoxDecoration(
                image: const DecorationImage(
                  image: NetworkImage(
                      'http://localhost:4000/static/upload/637969248afcffafb987d951//637969248afcffafb987d951-w-oCaV-gift.png'),
                  fit: BoxFit.cover,
                ),
                color: null,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                border: null,
              ),
              child: null,
            ),
          ),
        ],
      ),
    );
  }
}
