import 'package:flutter/material.dart';
import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/loaders/base_loader_controller.dart';
import 'package:provider/provider.dart';

class BaseLoader<T extends BaseLoaderController> extends StatefulWidget {
  final Function(T controller)? onReady;
  final Function(T controller)? onDispose;
  final Widget Function(BuildContext context, T controller, Widget? child)
      builder;
  const BaseLoader({
    Key? key,
    this.onDispose,
    this.onReady,
    required this.builder,
  }) : super(key: key);

  @override
  _BaseLoaderState<T> createState() => _BaseLoaderState<T>();
}

class _BaseLoaderState<T extends BaseLoaderController>
    extends State<BaseLoader<T>> {
  late T controller;

  @override
  void initState() {
    super.initState();
    controller = locator<T>();
    if (widget.onReady != null) widget.onReady!(controller);
  }

  @override
  void dispose() {
    if (widget.onDispose != null) widget.onDispose!(controller);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => controller,
      child: Consumer<T>(
        builder: widget.builder,
      ),
    );
  }
}
