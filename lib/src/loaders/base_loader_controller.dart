import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/service/navigation.service.dart';

enum LoaderState { Idle, Loading, Error, Success }

class BaseLoaderController extends ChangeNotifier {
  LoaderState _state = LoaderState.Idle;
  LoaderState get state => _state;
  setState(LoaderState state, [bool notify = true]) {
    _state = state;
    notifyListeners();
  }

  GlobalKey<NavigatorState> get navigationKey =>
      locator<NavigationService>().navigationKey;
}
