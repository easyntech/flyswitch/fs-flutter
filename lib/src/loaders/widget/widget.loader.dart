import 'package:flutter/material.dart';
import 'package:flyswitch/src/loaders/base_loader.dart';
import 'package:flyswitch/src/loaders/base_loader_controller.dart';
import 'package:flyswitch/src/loaders/widget/widget_loader.controller.dart';
import 'package:flyswitch/src/managers/notifier.manager.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';

class WidgetLoader extends StatelessWidget {
  final Widget Function()? loadingBuilder;
  final String id;
  const WidgetLoader({
    super.key,
    this.loadingBuilder,
    required this.id,
  });

  @override
  Widget build(BuildContext context) {
    return BaseLoader<WidgetLoaderController>(
      onReady: (controller) => controller.loadWidget(id),
      builder: (context, controller, child) {
        Widget child;
        switch (controller.state) {
          case LoaderState.Loading:
            child = loadingBuilder?.call() ??
                const Center(
                  child: CircularProgressIndicator(),
                );
            break;
          case LoaderState.Error:
            child = Container();
            break;
          case LoaderState.Success:
            child = controller.widgetJson != null
                ? JsonWidgetData.fromDynamic(controller.widgetJson)!
                    .build(context: controller.navigationKey.currentContext!)
                : Container();
            break;
          default:
            child = Container();
        }
        return NotifierManager(child: child);
      },
    );
  }
}
