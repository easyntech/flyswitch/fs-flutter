import 'package:flutter/foundation.dart';
import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/loaders/base_loader_controller.dart';
import 'package:flyswitch/src/service/widgets.service.dart';

class WidgetLoaderController extends BaseLoaderController {
  final WidgetsService _widgetsService = locator<WidgetsService>();
  Map<String, dynamic>? _widgetJson;
  Map<String, dynamic>? get widgetJson => _widgetJson;

  Future<void> loadWidget(String id) async {
    setState(LoaderState.Loading);
    try {
      _widgetJson = await _widgetsService.fetchWidgetById(id);
      setState(LoaderState.Success);
    } catch (e) {
      setState(LoaderState.Error);
    }
  }
}
