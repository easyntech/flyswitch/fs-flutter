import 'package:flutter/material.dart';
import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/service/notifier.service.dart';

class NotifierManager extends StatefulWidget {
  final Widget child;
  const NotifierManager({super.key, required this.child});

  @override
  State<NotifierManager> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<NotifierManager> {
  final NotifierService _notifierService = locator<NotifierService>();
  @override
  void initState() {
    super.initState();
    _notifierService.registerSnackBarListener(_showSnackBar);
  }

  _showSnackBar(SnackBarMsgOptions options) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(options.msg, style: options.textStyle),
        backgroundColor: options.bgColor,
        duration: Duration(seconds: options.duration ?? 3),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
