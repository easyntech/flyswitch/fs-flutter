import 'package:flutter/foundation.dart';

class DialogRequest {
  final String title;
  final String description;
  final String buttonTitle;
  final String? cancelTitle;
  final void Function()? onConfirm;
  final void Function()? onCancel;

  DialogRequest({
    required this.title,
    required this.description,
    required this.buttonTitle,
    required this.cancelTitle,
    this.onConfirm,
    this.onCancel,
  });
}

class DialogResponse {
  final String? fieldOne;
  final String? fieldTwo;
  final bool confirmed;

  DialogResponse({
    this.fieldOne,
    this.fieldTwo,
    required this.confirmed,
  });
}
