class ResponsePagination {
  final String prev;
  final String current;
  final String next;

  ResponsePagination(this.prev, this.current, this.next);

  factory ResponsePagination.fromJSON(Map<String, dynamic> data) {
    return ResponsePagination(data['prev'], data['current'], data['next']);
  }
}

class HttpResponse<D extends Map<String, dynamic>,
    R extends Map<String, dynamic>> {
  final int status;
  final D? data;
  final R? related;
  final ResponsePagination? pagination;
  late final bool isSuccess;

  HttpResponse(this.status, this.data, {this.pagination, this.related}) {
    isSuccess = [200, 201, 204].contains(status);
  }
}
