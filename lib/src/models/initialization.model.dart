library flyswitch;

class FlySwitchInitOptions {
  final String apiKey;
  FlySwitchInitOptions({
    required this.apiKey,
  });
}
