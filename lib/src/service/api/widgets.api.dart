import 'package:flyswitch/src/models/http_response.model.dart';
import 'package:flyswitch/src/utils/api.util.dart';

class WidgetsApi {
  Future<HttpResponse> fetchWidgetById(String id) async {
    final response = await getHttpClient().get('/widgets/$id/flutter');
    if (response.statusCode == 200) {
      return HttpResponse(response.statusCode!, response.data!['data'],
          related: response.data!['related']);
    } else {
      throw Exception('Failed to load widget');
    }
  }
}
