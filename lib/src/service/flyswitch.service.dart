import 'package:flutter/material.dart'
    show GlobalKey, NavigatorState, showDialog, Colors;
import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/loaders/widget/widget.loader.dart';
import 'package:flyswitch/src/service/json_registery.service.dart';
import 'package:flyswitch/src/service/navigation.service.dart';
import 'package:flyswitch/src/service/widgets.service.dart';

class FlySwitchService {
  String? _apiKey;
  String? get apiKey => _apiKey;
  FlySwitchService._();
  static final FlySwitchService _instance = FlySwitchService._();
  factory FlySwitchService() => _instance;

  void init(String apiKey) {
    _apiKey = apiKey;
    locator<JsonRegistryService>().setupRegistry();
  }

  Future<void> load(String id) async {
    await locator<WidgetsService>().fetchWidgetById(id);
  }

  show(String id) {
    showDialog(
      context: navigationKey.currentContext!,
      builder: (_) => WidgetLoader(id: id),
      useSafeArea: false,
      barrierColor: Colors.transparent,
    );
  }

  GlobalKey<NavigatorState> get navigationKey =>
      locator<NavigationService>().navigationKey;
}
