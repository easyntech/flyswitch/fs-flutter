import 'package:flyswitch/src/service/json_registry/custom_builders/dialog_custom_json_builder.dart';
import 'package:flyswitch/src/service/json_registry/json_registry_functions.dart';
import 'package:flyswitch/src/service/json_registry/json_registry_values.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';

class JsonRegistryService {
  final r = JsonWidgetRegistry.instance;

  _setupRegistryFunction() {
    r.registerFunction('launchUrl', launchUrlRegistryFunction);
    r.registerFunction('launchApp', launchAppRegistryFunction);
    r.registerFunction('shareText', shareTextRegistryFunction);
    r.registerFunction('copyToClipboard', copyToClipboardRegistryFunction);
    r.registerFunction("getSize", getSize);
    r.registerFunction("getHorizontalSize", getHorizontalSize);
    r.registerFunction("getVerticalSize", getVerticalSize);
    r.registerFunction("closeModal", closeModalRegistryFunction);
  }

  _setupRegistryValues() {
    setScreenSize();
    setPlaygroundSize(375, 665);
    setCellSize();
    setSizeRatio();
    setDpr();
  }

  _setupRegistryCustomBuilders() {
    registerDialogCustomBuilder();
  }

  setupRegistry() {
    _setupRegistryFunction();
    _setupRegistryValues();
    _setupRegistryCustomBuilders();
  }
}
