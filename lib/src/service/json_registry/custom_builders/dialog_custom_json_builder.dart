import 'package:child_builder/src/child_widget_builder.dart';
import 'package:flutter/material.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';

class _DialogBuilder extends JsonWidgetBuilder {
  final double elevation;
  final String backgroundColor;
  final double insetPadding;

  _DialogBuilder({
    this.elevation = 0,
    this.backgroundColor = "transparent",
    this.insetPadding = 0,
  }) : super(numSupportedChildren: 1);

  static const type = 'dialog';

  static _DialogBuilder fromDynamic(
    dynamic map, {
    JsonWidgetRegistry? registry,
  }) {
    _DialogBuilder result;

    result = _DialogBuilder(
      elevation: map['elevation'] ?? 0.0,
      backgroundColor: map['backgroundColor'] ?? Colors.transparent,
      insetPadding: map['insetPadding'] ?? 0.0,
    );
    return result;
  }

  @override
  Widget buildCustom({
    ChildWidgetBuilder? childBuilder,
    required BuildContext context,
    required JsonWidgetData data,
    Key? key,
  }) {
    // assert(
    //   data.children?.isNotEmpty != true,
    //   '[SvgBuilder] does not support children.',
    // );
    return Dialog(
      elevation: elevation,
      backgroundColor: backgroundColor == "transparent"
          ? Colors.transparent
          : Color(int.parse("0xFF${backgroundColor.replaceAll("#", '')}")),
      insetPadding: EdgeInsets.all(insetPadding),
      child: data.children?.isNotEmpty == true
          ? data.children![0].build(
              childBuilder: childBuilder,
              context: context,
            )
          : Container(),
    );
  }
}

registerDialogCustomBuilder() {
  final r = JsonWidgetRegistry.instance;
  r.registerCustomBuilder(
    _DialogBuilder.type,
    const JsonWidgetBuilderContainer(
      builder: _DialogBuilder.fromDynamic,
    ),
  );
}
