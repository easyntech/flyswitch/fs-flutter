import 'dart:convert';

import 'package:external_app_launcher/external_app_launcher.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/managers/notifier.manager.dart';
import 'package:flyswitch/src/service/navigation.service.dart';
import 'package:flyswitch/src/service/notifier.service.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

closeModalRegistryFunction({
  required List<dynamic>? args,
  required JsonWidgetRegistry registry,
}) {
  return () async {
    locator<NavigationService>().pop();
  };
}

launchUrlRegistryFunction({
  required List<dynamic>? args,
  required JsonWidgetRegistry registry,
}) {
  return () async {
    if (args != null && args.isNotEmpty) {
      final url = args[0];
      if (url is String && await canLaunchUrl(Uri.parse(url))) {
        await launchUrl(Uri.parse(url));
      }
    }
  };
}

launchAppRegistryFunction({
  required List<dynamic>? args,
  required JsonWidgetRegistry registry,
}) {
  return () async {
    if (args != null && args.isNotEmpty) {
      final androidPN = args[0];
      final iosURLSchema = args[1];
      final appStoreLink = args[2];
      return await LaunchApp.openApp(
        androidPackageName: androidPN is String ? androidPN : null,
        iosUrlScheme: iosURLSchema is String ? iosURLSchema : null,
        appStoreLink: appStoreLink is String ? appStoreLink : null,
      );
    }
  };
}

shareTextRegistryFunction({
  required List<dynamic>? args,
  required JsonWidgetRegistry registry,
}) {
  return () async {
    if (args != null && args.isNotEmpty) {
      final text = args[0];
      final subject = args.length > 1 && args[1] is String ? args[1] : null;
      if (text is String) {
        final box = registry.navigatorKey?.currentContext?.findRenderObject()
            as RenderBox?;
        if (box != null) await Share.share(text, subject: subject);
      }
    }
  };
}

copyToClipboardRegistryFunction({
  required List<dynamic>? args,
  required JsonWidgetRegistry registry,
}) {
  return () async {
    if (args != null && args.isNotEmpty) {
      final text = args[0];
      if (text is String) {
        Map<String, dynamic>? messages =
            args.length > 1 ? jsonDecode(args[1]) : null;
        try {
          final box = registry.navigatorKey?.currentContext?.findRenderObject()
              as RenderBox?;
          await Clipboard.setData(ClipboardData(text: text));
          locator<NotifierService>().showSuccessSnackBar(
              messages?['success'] ?? "Copied to clipboard");
        } catch (e) {
          locator<NotifierService>().showErrorSnackBar(
              messages?['error'] ?? "Failed to copy to clipboard");
        }
        locator<NavigationService>().pop();
      }
    }
  };
}

getHorizontalSize({
  required List<dynamic>? args,
  required JsonWidgetRegistry registry,
}) {
  if (args != null && args.isNotEmpty) {
    final px = args[0];
    if (px is double || px is int) {
      final window = WidgetsBinding.instance.window;
      final size = window.physicalSize / window.devicePixelRatio;
      int screenWidth = 375;
      return px * (size.width / screenWidth);
    }
  }
}

getVerticalSize({
  required List<dynamic>? args,
  required JsonWidgetRegistry registry,
}) {
  if (args != null && args.isNotEmpty) {
    final px = args[0];
    if (px is double || px is int) {
      final window = WidgetsBinding.instance.window;
      final size = window.physicalSize / window.devicePixelRatio;
      int screenHeight = 665;
      return px * (size.height / screenHeight);
    }
  }
}

getSize({
  required List<dynamic>? args,
  required JsonWidgetRegistry registry,
}) {
  if (args != null && args.isNotEmpty) {
    final px = args[0];
    if (px is double || px is int) {
      final window = WidgetsBinding.instance.window;
      final size = window.physicalSize / window.devicePixelRatio;
      int screenHeight = 665;
      int screenWidth = 375;
      final height = px * (size.height / screenHeight);
      final width = px * (size.width / screenWidth);

      if (height > width) {
        return height;
      } else {
        return width;
      }
    }
  }
}
