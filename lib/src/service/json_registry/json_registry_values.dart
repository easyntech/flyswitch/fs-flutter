import 'package:flutter/widgets.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';

setScreenWidth() {
  final r = JsonWidgetRegistry.instance;
  final window = WidgetsBinding.instance.window;
  final size = window.physicalSize / window.devicePixelRatio;
  final screenWidth = size.width;
  r.setValue("screenWidth", screenWidth);
}

setScreenHeight() {
  final r = JsonWidgetRegistry.instance;
  final window = WidgetsBinding.instance.window;
  final size = window.physicalSize / window.devicePixelRatio;
  final screenHeight = size.height;
  r.setValue("screenHeight", screenHeight);
}

setScreenSize() {
  setScreenWidth();
  setScreenHeight();
}

setPlaygroundWidth(double width) {
  final r = JsonWidgetRegistry.instance;
  r.setValue("playgroundWidth", width);
}

setPlaygroundHeight(double height) {
  final r = JsonWidgetRegistry.instance;
  r.setValue("playgroundHeight", height);
}

setPlaygroundSize(double width, double height) {
  setPlaygroundWidth(width);
  setPlaygroundHeight(height);
}

setDpr() {
  final r = JsonWidgetRegistry.instance;
  final window = WidgetsBinding.instance.window;
  final dpr = window.devicePixelRatio;
  r.setValue("dpr", dpr);
}

setGridColumns(int columns, {bool updateCellWidth = true}) {
  final r = JsonWidgetRegistry.instance;
  r.setValue("gridColumns", columns);
  if (updateCellWidth) {
    setCellWidth();
  }
}

setGridRows(int rows, {bool updateCellHeight = true}) {
  final r = JsonWidgetRegistry.instance;
  r.setValue("gridRows", rows);
  if (updateCellHeight) {
    setCellHeight();
  }
}

setCellWidth() {
  final r = JsonWidgetRegistry.instance;
  // get playgroundWidth, if not exist throw error
  double? playgroundWidth = r.getValue("playgroundWidth");
  if (playgroundWidth == null) throw Exception("playgroundWidth is not set");
  // check if gridColumns is set if not set it to 100
  int? gridColumns = r.getValue("gridColumns");
  if (gridColumns == null) {
    r.setValue("gridColumns", 100);
    gridColumns = 100;
  }
  final cellWidth = r.getFunction("getHorizontalSize")?.call(
    args: [playgroundWidth / gridColumns],
    registry: r,
  );
  r.setValue("cellWidth", cellWidth);
}

setCellHeight() {
  final r = JsonWidgetRegistry.instance;
  // get playgroundHeight, if not exist throw error
  double? playgroundHeight = r.getValue("playgroundHeight");
  if (playgroundHeight == null) throw Exception("playgroundHeight is not set");
  // check if gridRows is set if not set it to 100
  int? gridRows = r.getValue("gridRows");
  if (gridRows == null) {
    r.setValue("gridRows", 100);
    gridRows = 100;
  }
  final cellHeight = r.getFunction("getVerticalSize")?.call(
    args: [playgroundHeight / gridRows],
    registry: r,
  );
  r.setValue("cellHeight", cellHeight);
}

setCellSize() {
  setCellWidth();
  setCellHeight();
}

setGridSize(int columns, int rows) {
  setGridColumns(columns, updateCellWidth: false);
  setGridRows(rows, updateCellHeight: false);
  setCellSize();
}

setSwToPw() {
  final r = JsonWidgetRegistry.instance;
  // get screenWidth, if not exist throw error
  double? screenWidth = r.getValue("screenWidth");
  if (screenWidth == null) throw Exception("screenWidth is not set");

  // check if playgroundWidth is set if not set it to 375
  double? playgroundWidth = r.getValue("playgroundWidth");
  if (playgroundWidth == null) {
    r.setValue("playgroundWidth", 375);
    playgroundWidth = 375;
  }

  final swToPw = (screenWidth) / playgroundWidth;
  r.setValue("swToPw", swToPw);
}

setShToPh() {
  final r = JsonWidgetRegistry.instance;
  // get screenHeight, if not exist throw error
  final screenHeight = r.getValue("screenHeight");
  if (screenHeight == null) throw Exception("screenHeight is not set");
  // check if playgroundHeight is set if not set it to 665
  double? playgroundHeight = r.getValue("playgroundHeight");
  if (playgroundHeight == null) {
    r.setValue("playgroundHeight", 665);
    playgroundHeight = 665;
  }

  final shToPh = (screenHeight) / playgroundHeight;
  r.setValue("shToPh", shToPh);
}

setSizeRatio() {
  final r = JsonWidgetRegistry.instance;
  final sizeRatio = r.getFunction("getSize")?.call(
    args: [1],
    registry: r,
  );
  r.setValue("sizeRatio", sizeRatio);
  setSwToPw();
  setShToPh();
}
