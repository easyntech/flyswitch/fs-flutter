import 'package:flutter/material.dart';

class NavigationService {
  final GlobalKey<NavigatorState> _navigationKey = GlobalKey<NavigatorState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  GlobalKey<NavigatorState> get navigationKey => _navigationKey;
  GlobalKey<ScaffoldState> get globalScaffoldKey => _scaffoldKey;

  void pop([bool dialog = false]) {
    var currentState = _navigationKey.currentState!;
    if (dialog) {
      return Navigator.of(currentState.context, rootNavigator: true).pop();
    }

    currentState.pop();
  }

  Future<dynamic> popTillAndNavTo(String till, String routeName,
      {dynamic arguments}) {
    return _navigationKey.currentState!.pushNamedAndRemoveUntil(
        routeName, (route) => route.isFirst,
        arguments: arguments);
  }

  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) {
    return _navigationKey.currentState!
        .pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> navigateToWithReplacement(String routeName,
      {dynamic arguments}) {
    return _navigationKey.currentState!
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  Future<dynamic> popAndNavigateTo(String routeName, {dynamic arguments}) {
    return _navigationKey.currentState!
        .popAndPushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> navigateToWithRoute(MaterialPageRoute route) {
    return _navigationKey.currentState!.push(route);
  }

  Future<dynamic> navigateToReplacementRoute(MaterialPageRoute route) {
    return _navigationKey.currentState!.pushReplacement(route);
  }
}
