import 'package:flutter/material.dart';

enum MsgStatus { Success, Error, Info, Custom }

class SnackBarMsgOptions {
  final MsgStatus? status;
  final String msg;
  final TextStyle? textStyle;
  final Color? bgColor;
  final int? duration;
  SnackBarMsgOptions(
      {this.status = MsgStatus.Success,
      required this.msg,
      this.textStyle = const TextStyle(color: Colors.white),
      this.bgColor = Colors.green,
      this.duration = 3});
}

class NotifierService {
  late Function(SnackBarMsgOptions) _showSnackBar;
  void registerSnackBarListener(Function(SnackBarMsgOptions) showSnackBar) {
    _showSnackBar = showSnackBar;
  }

  showSuccessSnackBar(String msg, [int? duration]) {
    _showSnackBar(SnackBarMsgOptions(msg: msg, duration: duration));
  }

  showErrorSnackBar(String msg, [int? duration]) {
    _showSnackBar(
      SnackBarMsgOptions(
        msg: msg,
        bgColor: Colors.red,
        status: MsgStatus.Error,
        duration: duration,
      ),
    );
  }

  showInfoSnackBar(String msg, [int? duration]) {
    _showSnackBar(
      SnackBarMsgOptions(
        msg: msg,
        bgColor: Colors.blue,
        status: MsgStatus.Info,
        duration: duration,
      ),
    );
  }

  showCustomSnackBar(SnackBarMsgOptions options) {
    _showSnackBar(options);
  }
}
