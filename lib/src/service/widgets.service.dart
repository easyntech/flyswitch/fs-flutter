import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/service/api.service.dart';
import 'package:flyswitch/src/service/api/widgets.api.dart';

class WidgetsService {
  final WidgetsApi _api = locator<API>().widgets;

  final Map<String, Map<String, dynamic>> _widgets = {};

  Map<String, dynamic>? _widgetJson;
  Map<String, dynamic>? get widgetJson => _widgetJson;

  Future<Map<String, dynamic>?> fetchWidgetById(String id) async {
    if (_widgets.containsKey(id)) {
      _widgetJson = _widgets[id];
      return _widgetJson;
    }
    final response = await _api.fetchWidgetById(id);
    if (response.status == 200) {
      _widgetJson = response.data;
      _widgets[id] = _widgetJson!;
      return _widgetJson;
    } else {
      throw Exception('Failed to load widget');
    }
  }
}
