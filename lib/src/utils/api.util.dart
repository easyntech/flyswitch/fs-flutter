import 'package:dio/dio.dart';
import 'package:flyswitch/locator.dart';
import 'package:flyswitch/src/service/flyswitch.service.dart';
import 'package:flyswitch/src/service/navigation.service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HttpBuilder {
  final NavigationService _ns = locator<NavigationService>();
  final FlySwitchService _flySwitchService = locator<FlySwitchService>();
  final Dio _dio;

  HttpBuilder(this._dio);

  instance() {
    _dio.interceptors.clear();
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      if (_flySwitchService.apiKey != null &&
          _flySwitchService.apiKey!.isNotEmpty) {
        options.headers['x-api-key'] = _flySwitchService.apiKey;
      }

      handler.next(options);
    }, onError: (err, handler) {
      handler.next(err);
    }));

    return _dio;
  }
}

Dio getHttpClient() {
  return HttpBuilder(
    Dio(
      BaseOptions(
        baseUrl: "https://api.flyswitch.io/v1",
      ),
    ),
  ).instance();
}
